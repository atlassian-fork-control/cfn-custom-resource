# get_ami

This custom resource fetch AMI ID according to input arguments.

## Inputs

* Name - Image name
* Owner - Image owner
* Latest - if True returns latest Id if multiple images exists _(optional)_
* Tags - Additional tags _(optional)_

## Outputs

* ImageId - Image id

# Custom Resource definition

```yaml
AMIResolver: 
  Type: "Custom::Resource"
  Properties: 
    ServiceToken: !Sub arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:function:${LambdaFunctionName}
    Name: my_ami
    Owner: 123456789
    Latest: TRUE
```


# Folder structure
The lambda should follow the standard lambda package structure:

```
.
├── README.md
├── cfn_custom_resource
│   ├── __init__.py
│   └── decorator.py
└── get_ami.py
```
